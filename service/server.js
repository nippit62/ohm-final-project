const express = require("express");
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");

app.use(cors());


mongoose.connect('mongodb+srv://ohmohmohma30412:ohm0966477158@ohm1918.maf3klm.mongodb.net/JJ?retryWrites=true&w=majority', {
    useNewUrlParser: true
});

//สร้าง database schema
const Premium = mongoose.model(
    'wits', new mongoose.Schema({ _uuid: String, _pm2_5: String }));



app.get('/', (req, res) => {
    res.json({ message: 'ok' });
});

app.get('/wits', (req, res) => {
    Premium.find({}).then((wits) => res.json(wits)).catch(error => res.status(400).json({ message: 'Some thing went wrong' }))
});

app.get('/wits/1', (req, res) => {
    Premium.find({ _uuid: "PM_0001" }).then((wits) => res.json(wits)).catch(error => res.status(400).json({ message: 'Some thing went wrong' }))
});

app.get('/wits/2', (req, res) => {
    const now = new Date()
    const lastHour = now.setHours(now.getHours() - 1);
    Premium.find({ _uuid: "PM_0002", _time: { $gte: lastHour, $lte: Date.now() } }).then((wits) => res.json(wits)).catch(error => res.status(400).json({ message: 'Some thing went wrong' }))
});

app.get('/avg', (req, res) => {
    const now = new Date()
    const lastHour = now.setHours(now.getHours() - 1);

    Premium.aggregate(
        [
            {
                $match: { _time: { $gte: lastHour, $lte: Date.now() } }
            },
            {
                $group:
                {
                    _id: "$_uuid",
                    avg25: { $avg: { $toInt: "$_pm2_5" } },
                    avg10: { $avg: { $toInt: "$_pm10" } }
                }
            }
        ]
    ).then((result) => res.json(result)).catch(error => res.status(400).json({ message: 'Some thing went wrong' }))
})

app.listen(9999, () => console.log('ok'));