import {
  useLoadScript,
  GoogleMap,
  MarkerF,
  CircleF,
} from "@react-google-maps/api";
import type { NextPage } from "next";
import { useMemo, useState, useEffect } from "react";
// import usePlacesAutocomplete, {
//   getGeocode,
//   getLatLng,
// } from "use-places-autocomplete";
import styles from "../styles/Home.module.css";
import { io } from "socket.io-client";
import { Line } from "react-chartjs-2";
// import ReactApexChart from "react-apexcharts";
import dynamic from "next/dynamic";
import axios from "axios";

const ReactApexChart = dynamic(() => import("react-apexcharts"), {
  ssr: false,
});

// import Chart from "chart.js/auto";
// import { CategoryScale } from "chart.js";

// Chart.register(CategoryScale);

const socket = io("http://3.1.133.90:8081/", { transports: ["websocket"] });

const Home: NextPage = () => {
  const [lat, setLat] = useState(27.672932021393862);
  const [lng, setLng] = useState(85.31184012689732);
  const [selected, setSelected] = useState(0);
  const [avgData, setAvgData] = useState([]);

  const libraries = useMemo(() => ["places"], []);
  const mapCenter = useMemo(() => ({ lat: lat, lng: lng }), [lat, lng]);

  const [dataList, setDatalist] = useState([]);

  const whiteContainer = useMemo(
    () => ({ lat: 16.738043, lng: 100.195036 }),
    []
  );
  const ohmRoom = useMemo(() => ({ lat: 16.751822, lng: 100.197467 }), []);

  const pm25Title: string[] = [
    "22",
    "22",
    "22",
    "22",
    "22",
    "22",
    "22",
    "22",
    "22",
    "22",
  ];

  var pm25List_test: number[][] = [[Date.now(),0]];
  var pm10List_test: number[][] = [[Date.now(),0]];

  var pm25List_10: number[][] = [[Date.now(),0]];
  var pm10List_10: number[][] = [[Date.now(),0]];

  var pm25List_25: number[][] = [[Date.now(),0]];
  var pm10List_25: number[][] = [[Date.now(),0]];

  socket.on("data", (data) => {
    console.log(data["_time"])
    if (data["_uuid"] == "PM_0001") {
      if (pm25List_10.length >= 6) {
        pm25List_10.shift();
      }

      if (pm10List_10.length >= 6) {
        pm10List_10.shift();
      }
      

      pm25List_10.push([Date.now(),parseInt(data["_pm2_5"])]);
      pm10List_10.push([Date.now(),parseInt(data["_pm10"])]);
      // console.log(pm25List_10, pm10List_10);
    }

    if (data["_uuid"] == "PM_0002") {
      if (pm25List_25.length >= 6) {
        pm25List_25.shift();
      }

      if (pm10List_25.length >= 6) {
        pm10List_25.shift();
      }

      pm25List_25.push([Date.now(),parseInt(data["_pm2_5"])]);
      pm10List_25.push([Date.now(),parseInt(data["_pm10"])]);
      // console.log(pm25List_25, pm10List_25);
    }
  });

  const mapOptions = useMemo<google.maps.MapOptions>(
    () => ({
      disableDefaultUI: true,
      clickableIcons: true,
      scrollwheel: false,
    }),
    []
  );

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: "AIzaSyBrteClS8p3vi2OHFqvGsUspN9LYuJd_t8" as string,
    libraries: libraries as any,
  });

  // const [data, updateData] = useState([1, 2, 3, 4, 5, 6]);
  const [dataNumber, updateDataNumber] = useState(0);
  const [titles, updateTitles] = useState("TITLE");

  var lineData_1: number[][] = [];
  var lineData_2: number[][] = [];
  // var dataNumber = 0;

  const series_10 = [
    {
      data: lineData_1.slice(),
    },
  ];

  const series_25 = [
    {
      data: lineData_2.slice(),
    },
  ];

  const options_10 = {
    chart: {
      id: "pm10",
      // height: 350,
      type: "line",
      animations: {
        enabled: true,
        easing: "fast",
        dynamicAnimation: {
          speed: 1000,
        },
      },
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    dataLabels: {
      enabled: true,
      enabledOnSeries: false,

      textAnchor: "middle",
      distributed: false,
      offsetX: 0,
      offsetY: 0,
      style: {
        fontSize: "14px",
        fontFamily: "Helvetica, Arial, sans-serif",
        fontWeight: "bold",
        colors: undefined,
      },
      background: {
        enabled: true,
        foreColor: "#fff",
        padding: 4,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: "#fff",
        opacity: 0.9,
        dropShadow: {
          enabled: false,
          top: 1,
          left: 1,
          blur: 1,
          color: "#000",
          opacity: 0.45,
        },
      },
      dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: "#000",
        opacity: 0.45,
      },
    },
    stroke: {
      curve: "smooth",
    },
    title: {
      text: "PM10",
      align: "left",
    },
    markers: {
      size: 0,
    },
    xaxis: {
      labels: {
        formatter: function (value:number, timestamp:number) {
          return new Date(value).toLocaleTimeString() // The formatter function overrides format property
        }, 
      }
    },
    // xaxis: {
    //   labels: {
    //     show: true,
    //   },
    //   max: 6,
    // },
    yaxis: {
      min: 0,
    },
    legend: {
      show: false,
    },
  };

  const options_25 = {
    chart: {
      id: "pm25",
      // height: 350,
      type: "line",
      animations: {
        enabled: true,
        easing: "linear",
        dynamicAnimation: {
          speed: 1000,
        },
      },
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    dataLabels: {
      enabled: true,
      enabledOnSeries: true,

      textAnchor: "middle",
      distributed: false,
      offsetX: 0,
      offsetY: 0,
      style: {
        fontSize: "14px",
        fontFamily: "Helvetica, Arial, sans-serif",
        fontWeight: "bold",
        colors: undefined,
      },
      background: {
        enabled: true,
        foreColor: "#fff",
        padding: 4,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: "#fff",
        opacity: 0.9,
        dropShadow: {
          enabled: false,
          top: 1,
          left: 1,
          blur: 1,
          color: "#000",
          opacity: 0.45,
        },
      },
      dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: "#000",
        opacity: 0.45,
      },
    },
    stroke: {
      curve: "smooth",
    },
    title: {
      text: "PM25",
      align: "left",
    },
    markers: {
      size: 0,
    },
    xaxis: {
      labels: {
        formatter: function (value:number, timestamp:number) {
          return new Date(value).toLocaleTimeString() // The formatter function overrides format property
        }, 
      }
    },
    yaxis: {
      min: 0,
    },
    legend: {
      show: false,
    },
  };

  useEffect(() => {
    const FetchAverage = async () => {
      try {
        const response = await axios.get(`http://localhost:9999/avg`);
        console.log(response);
        setAvgData(response.data);
      } catch (err) {
        console.error(err);
      }
    };
    FetchAverage();
  }, []);

  const handleAverageData = (uuid: string, onlyLabel: boolean) => {
    const found: any = avgData.find((obj) => obj["_id"] === uuid);
    if (!onlyLabel) {
      return found;
    }
    const pmavg25: number = found?.avg25;
    return Math.round(pmavg25);
  };

  useEffect(() => {
    console.log("GENERATE");
    const interval = setInterval(() => {
      console.log("LOOP");
      const val = Math.floor(Math.random() * (100 - 30 + 1)) + 30;
      const val2 = Math.floor(Math.random() * (100 - 30 + 1)) + 30;

      pm10List_test.push([Date.now(),val]);
      pm25List_test.push([Date.now(),val2]);

      if (selected == 1) {
        lineData_1 = pm10List_10.slice(Math.max(pm10List_10.length - 5, 0));
        lineData_2 = pm25List_10.slice(Math.max(pm25List_10.length - 5, 0));
      } else if (selected == 2) {
        lineData_1 = pm10List_25.slice(Math.max(pm10List_25.length - 5, 0));
        lineData_2 = pm25List_25.slice(Math.max(pm25List_25.length - 5, 0));
      } else {
        lineData_1 = pm10List_test.slice(Math.max(pm10List_test.length - 5, 0));
        lineData_2 = pm25List_test.slice(Math.max(pm25List_test.length - 5, 0));
      }

      // if (pm25List_10.length > 6) {
      //   pm25List_10.shift();
      // }

      // if (pm10List_10.length > 6) {
      //   pm10List_10.shift();
      // }

      // if (pm25List_25.length > 6) {
      //   pm25List_25.shift();
      // }

      // if (pm10List_25.length > 6) {
      //   pm10List_25.shift();
      // }

      if (lineData_1.length > 6) {
        // lineData_1.shift();
        lineData_1.shift();
      }

      if (lineData_2.length > 6) {
        // lineData_2.shift();
        lineData_2.shift();
      }
      console.log(lineData_1, lineData_2);
      ApexCharts.exec("pm10", "updateSeries", [
        {
          data: lineData_1.slice(Math.max(lineData_1.length - 5, 0)),
        },
      ]);
      ApexCharts.exec("pm25", "updateSeries", [
        {
          data: lineData_2.slice(Math.max(lineData_2.length - 5, 0)),
        },
      ]);
    }, 3000);

    return () => {
      window.clearInterval(interval); // clear the interval in the cleanup function
      window.dispatchEvent(new Event("resize"));
      ``;
    };
  }, [lineData_1, lineData_2,selected]);

  // useEffect(() => {
  //   if (selected == 1) {
  //     lineData_1 = pm10List_10.slice(Math.max(pm10List_10.length - 5, 0));
  //     lineData_2 = pm25List_10.slice(Math.max(pm25List_10.length - 5, 0));
  //   } else if (selected == 2) {
  //     lineData_1 = pm10List_25.slice(Math.max(pm10List_25.length - 5, 0));
  //     lineData_2 = pm25List_25.slice(Math.max(pm25List_25.length - 5, 0));
  //   } else {
  //     lineData_1 = pm10List_test.slice(Math.max(pm10List_test.length - 5, 0));
  //     lineData_2 = pm25List_test.slice(Math.max(pm25List_test.length - 5, 0));
  //   }
  // }, [
  //   pm10List_10,
  //   pm25List_10,
  //   pm10List_25,
  //   pm25List_25,
  //   pm10List_test,
  //   pm25List_test,
  // ]);

  if (!isLoaded) {
    return <p>Loading...</p>;
  }

  const iconBase =
    "https://developers.google.com/maps/documentation/javascript/examples/full/images/";

  return (
    <div className={styles.homeWrapper}>
      <div className={styles.sidebar}>
        <div>
          {/* <Line data={data} width={400} height={400} options={options} /> */}
          <ReactApexChart options={options_10} series={series_10} type="line" />
          <ReactApexChart options={options_25} series={series_25} type="line" />
        </div>
        {/* <div>
            <div className={styles.text}>{dataNumber}</div>
            <ul>
              {lineData.map((number) => (
                <li className={styles.text}>{number}</li>
              ))}
            </ul>
          </div> */}
      </div>
      <GoogleMap
        options={mapOptions}
        zoom={14}
        center={whiteContainer}
        mapTypeId={google.maps.MapTypeId.ROADMAP}
        mapContainerStyle={{ width: "100vw", height: "100vh" }}
        onLoad={(map) => console.log("Map Loaded")}
      >
        {avgData.length != 0 && (
        //   <MarkerF
        //     position={whiteContainer}
        //     onLoad={() => console.log("Marker Loaded")}
        //     onClick={() => {
        //       setSelected(1);
        //       // lineData_1 = pm10List_10.slice(
        //       //   Math.max(pm10List_10.length - 5, 0)
        //       // );
        //       // lineData_2 = pm25List_10.slice(
        //       //   Math.max(pm25List_10.length - 5, 0)
        //       // );
        //       // console.log(lineData_1, lineData_2);
        //       // console.log(handleAverageData("PM_0001", true));
        //     }}
        //     title={"PM_0001"}
        //     icon={{
        //       // path: google.maps.SymbolPath.CIRCLE,
        //       url: iconBase + "info-i_maps.png",
        //       fillColor: "#EB00FF",
        //       scale: 7,
        //     }}
        //   />
        // ) : (
          <MarkerF
            position={whiteContainer}
            onLoad={() => console.log("Marker Loaded")}
            onClick={() => {
              setSelected(1);
              // lineData_1 = pm10List_10.slice(
              //   Math.max(pm10List_10.length - 5, 0)
              // );
              // lineData_2 = pm25List_10.slice(
              //   Math.max(pm25List_10.length - 5, 0)
              // );
              // console.log(lineData_1, lineData_2);
              // console.log(handleAverageData("PM_0001", true));
            }}
            title={"PM_0001"}
            icon={{
              path: google.maps.SymbolPath.CIRCLE,
              url: iconBase + "info-i_maps.png",
              fillColor: "#EB00FF",
              scale: 7,
            }}
            label={{
              text: `${handleAverageData("PM_0001", true)}`,
              color: "#000",
              fontSize: "50",
              fontWeight: "bold",
            }}
          />
        )}

        {avgData.length != 0 && (
        //   <MarkerF
        //     position={ohmRoom}
        //     onLoad={() => console.log("Marker Loaded")}
        //     onClick={() => {
        //       setSelected(2);
        //       // lineData_1 = pm10List_25.slice(
        //       //   Math.max(pm10List_25.length - 5, 0)
        //       // );
        //       // lineData_2 = pm25List_25.slice(
        //       //   Math.max(pm25List_25.length - 5, 0)
        //       // );
        //       // console.log(lineData_1, lineData_2);
        //       // console.log(handleAverageData("PM_0002", true));
        //     }}
        //     title={"PM_0002"}
        //     icon={{
        //       // path: google.maps.SymbolPath.CIRCLE,
        //       url: iconBase + "info-i_maps.png",
        //       fillColor: "#EB00FF",
        //       scale: 7,
        //     }}
        //   />
        // ) : (
          <MarkerF
            position={ohmRoom}
            onLoad={() => console.log("Marker Loaded")}
            onClick={() => {
              setSelected(2);
              // lineData_1 = pm10List_25.slice(
              //   Math.max(pm10List_25.length - 5, 0)
              // );
              // lineData_2 = pm25List_25.slice(
              //   Math.max(pm25List_25.length - 5, 0)
              // );
              // console.log(lineData_1, lineData_2);
              // console.log(handleAverageData("PM_0002", true));
            }}
            title={"PM_0002"}
            icon={{
              path: google.maps.SymbolPath.CIRCLE,
              url: iconBase + "info-i_maps.png",
              fillColor: "#EB00FF",
              scale: 7,
            }}
            label={{
              text: `${handleAverageData("PM_0002", true)}`,
              color: "#000",
              fontSize: "50",
              fontWeight: "bold",
            }}
          />
        )}

        <MarkerF
          position={{ lat: 16.746483, lng: 100.177471 }}
          onLoad={() => console.log("Marker Loaded")}
          onClick={() => {
            setSelected(0);
            // lineData_1 = pm10List_test.slice(
            //   Math.max(pm10List_test.length - 5, 0)
            // );
            // lineData_2 = pm25List_test.slice(
            //   Math.max(pm25List_test.length - 5, 0)
            // );
            // console.log(lineData_1, lineData_2);
          }}
          title={"PM_TEST"}
          icon={{
            // path: google.maps.SymbolPath.CIRCLE,
            url: iconBase + "info-i_maps.png",
            fillColor: "#EB00FF",
            scale: 7,
          }}
        />
      </GoogleMap>
    </div>
  );
};

export default Home;
